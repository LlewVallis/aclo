package org.astropeci.aclo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Signifies a method should be mapped to an action.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AcloAction {

    String actionName() default "%(method_name)";
    String responseMessage() default "%(return_value)";
    String description() default "no description";
    boolean async() default false;
}
