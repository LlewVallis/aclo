package org.astropeci.aclo.internal;

import java.util.function.Function;

/**
 * A utility class for building strings.
 */
public class StringConverter {

    public static <T, R> String convertIterable(Iterable<T> iterable, String separator, Function<T, R> map) {
        StringBuilder builder = new StringBuilder();
        for (T elem : iterable) {
            builder.append(map.apply(elem)).append(separator);
        }

        if (builder.length() > 0) {
            builder.replace(builder.length() - separator.length(), builder.length(), "");
        }

        return builder.toString();
    }

    public static String convertIterable(Iterable<?> iterable, String separator) {
        return convertIterable(iterable, separator, x -> x);
    }

    public static <T, R> String convertIterable(Iterable<T> iterable, Function<T, R> map) {
        return convertIterable(iterable, ", ", map);
    }

    public static String convertIterable(Iterable<?> iterable) {
        return convertIterable(iterable, ", ", x -> x);
    }
}
