package org.astropeci.aclo.internal;

import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A task run in the background.
 */
public class BackgroundTask {

    public static final String STATUS_DONE = "done";
    public static final String STATUS_RUNNING = "running";
    public static final String STATUS_AWAITING = "awaiting";

    private final AcloImpl api;
    private String name;
    private final Future<String> future;
    private final Object answerLock = new Object();

    public BackgroundTask(AcloImpl api, String name, Future<String> future) {
        this.api = api;
        this.name = name;
        this.future = future;
    }

    public String getName() {
        return name;
    }

    public Future<String> getFuture() {
        return future;
    }

    public AcloImpl getApi() {
        return api;
    }

    public String getStatus() {
        for (ApiEvent event : api.getEventQueue()) {
            if (event instanceof ApiAwaitingEvent) {
                return STATUS_AWAITING;
            }
        }

        if (future.isDone()) {
            return STATUS_DONE;
        } else {
            return STATUS_RUNNING;
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getAnswerLock() {
        return answerLock;
    }
}
