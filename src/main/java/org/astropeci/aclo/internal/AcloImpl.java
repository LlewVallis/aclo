package org.astropeci.aclo.internal;

import org.astropeci.aclo.Aclo;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Future;
import java.util.function.Supplier;

/**
 * An implementation for Aclo.
 */
public class AcloImpl implements Aclo {

    private List<ApiEvent> eventQueue = new CopyOnWriteArrayList<>();
    private Supplier<Boolean> canExitSupplier = null;
    private boolean silent = false;

    public AcloImpl() { }

    private AcloImpl(AcloImpl impl, boolean silent) {
        this.eventQueue = impl.eventQueue;
        this.canExitSupplier = impl.canExitSupplier;
        this.silent = silent;
    }

    @Override
    public void display(String msg) {
        if (!silent) {
            eventQueue.add(new ApiDisplayEvent(msg));
        }
    }

    @Override
    public Future<String> query(String msg) {
        CompletableFuture<String> future = new CompletableFuture<>();
        if (!silent) {
            eventQueue.add(new ApiQueryEvent(msg, future));
        }
        return future;
    }

    @Override
    public boolean canExit() {
        if (canExitSupplier != null) {
            return canExitSupplier.get();
        } else {
            return false;
        }
    }

    @Override
    public void requestExit() {
        if (canExit()) {
            eventQueue.add(new ApiExitEvent());
        }
    }

    @Override
    public Aclo silent() {
        return new AcloImpl(this, true);
    }

    public List<ApiEvent> getEventQueue() {
        return eventQueue;
    }

    public void setCanExitSupplier(Supplier<Boolean> canExitSupplier) {
        this.canExitSupplier = canExitSupplier;
    }
}
