package org.astropeci.aclo.internal;

/**
 * An event that corresponds to a display api request.
 */
public class ApiDisplayEvent extends ApiEvent {

    private final String msg;

    public ApiDisplayEvent(String msg) {
        this.msg = msg;
    }

    public String getMessage() {
        return msg;
    }
}
