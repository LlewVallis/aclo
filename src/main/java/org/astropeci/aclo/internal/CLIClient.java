package org.astropeci.aclo.internal;

import org.astropeci.aclo.Aclo;
import org.astropeci.aclo.Configuration;
import org.astropeci.aclo.MalformedArgumentException;
import org.astropeci.aclo.MissingActionException;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A client for the command line interface.
 */
public class CLIClient {

    private final ExecutorService executorService = Executors.newCachedThreadPool();

    private final Configuration config;
    private final WrappedImplementationManager implementationManager;

    private AtomicBoolean started = new AtomicBoolean();
    private AtomicBoolean stopped = new AtomicBoolean();
    private AtomicBoolean forceStop = new AtomicBoolean();

    public static final Iterable<String> reservedActions = Arrays.asList(
            "help",
            "exit",
            "forceExit",
            "status",
            "answer",
            "finish",
            "rename",
            "foreground",
            "background"
    );

    private final List<BackgroundTask> backgroundTasks = new CopyOnWriteArrayList<>();

    private final Object backgroundTasksWriteRenameLock = new Object();
    private final Object taskCreationLock = new Object();

    public CLIClient(Configuration config, WrappedImplementationManager implementationManager) {
        this.config = config;
        this.implementationManager = implementationManager;

        implementationManager.addAction(
                new ActionAdapter("exit", config.getExitActionDescription(), new ArrayList<>(), String.class) {
                    @Override
                    public Object invoke(Aclo api, Object obj, Object... args) {
                        synchronized (taskCreationLock) {
                            for (BackgroundTask bgTask : backgroundTasks) {
                                if (!bgTask.getStatus().equals(BackgroundTask.STATUS_DONE)) {
                                    return config.getCannotExitDueToBackgroundTasksMessage();
                                }
                            }

                            executeExitHook();
                            stopped.set(true);
                            return config.getFarewell();
                        }
                    }
                });

        implementationManager.addAction(
                new ActionAdapter("forceExit", config.getForceExitActionDescription(), new ArrayList<>(), Void.TYPE) {
                    @Override
                    public Object invoke(Aclo api, Object obj, Object... args) {
                        executeExitHook();
                        stopped.set(true);
                        forceStop.set(true);
                        return null;
                    }

                    @Override
                    public String getResponseMessage(String returnValue) {
                        return config.getFarewell();
                    }
                });

        implementationManager.addAction(
                new ActionAdapter("help", config.getHelpActionDescription(), new ArrayList<>(), Void.TYPE) {
                    @Override
                    public String getResponseMessage(String returnValue) {
                        return StringConverter.convertIterable(implementationManager.getActions(), "\n", action -> {
                            String requiredParams = StringConverter.convertIterable(action.getRequiredParams(),
                                    Class::getSimpleName);

                            if (requiredParams.isEmpty()) {
                                requiredParams = "void";
                            }

                            return String.format("(%s) %s (%s -> %s) - %s",
                                    action.isBackgroundTask() ? "background" : "foreground", action.getName(),
                                    requiredParams, action.getReturnType().getSimpleName(), action.getDescription());
                        });
                    }
        });

        implementationManager.addAction(
                new ActionAdapter("status", config.getStatusActionDescription(), new ArrayList<>(), Void.TYPE) {
                    @Override
                    public String getResponseMessage(String returnValue) {
                        String status = StringConverter.convertIterable(backgroundTasks, "\n",
                                task -> task.getName() + " - " + task.getStatus());

                        if (status.isEmpty()) {
                            status = config.getNoBackgroundTasksRunningMessage();
                        }

                        return status;
                    }
                }
        );

        implementationManager.addAction(
                new ActionAdapter("answer", config.getAnswerActionDescription(), Collections.singleton(String.class),
                        String.class) {
                    @Override
                    public String invoke(Aclo api, Object obj, Object... args) {
                        return answerWithTimeout((String) args[0], config.getAnswerTimeout());
                    }
                }
        );

        implementationManager.addAction(
                new ActionAdapter("finish", config.getFinishActionDescription(), Collections.singleton(String.class),
                        String.class) {
                    @Override
                    public String invoke(Aclo api, Object obj, Object... args) {
                        return answerWithTimeout((String) args[0], -1);
                    }
                }
        );

        implementationManager.addAction(
                new ActionAdapter("rename", config.getRenameActionDescription(),
                        Arrays.asList(String.class, String.class), String.class) {
                    @Override
                    public Object invoke(Aclo api, Object obj, Object... args) {
                        String taskName = (String) args[0];
                        String newName = (String) args[1];

                        for (BackgroundTask bgTask : backgroundTasks) {
                            if (bgTask.getName().equals(taskName)) {
                                synchronized (backgroundTasksWriteRenameLock) {
                                    for (BackgroundTask bgTask1 : backgroundTasks) {
                                        if (bgTask1.getName().equals(newName)) {
                                            return config.getBackgroundTaskAlreadyExistsMessage();
                                        }
                                    }

                                    bgTask.setName(newName);
                                    return config.getRenameCompleteMessage();
                                }
                            }
                        }

                        return config.getBackgroundTaskNotFoundMessage();
                    }
                }
        );

        implementationManager.addAction(new ActionAdapter("foreground", config.getForegroundActionDescription(),
                Collections.singleton(String.class), Void.TYPE) {
            @Override
            public Object invoke(Aclo api, Object obj, Object... args) {
                String[] inputElems = ((String) args[0]).split("-");

                String action = inputElems[0];

                String[] argsToPass = new String[inputElems.length - 1];
                System.arraycopy(inputElems, 1, argsToPass, 0, argsToPass.length);

                executeAction(action, false, false, argsToPass).ifPresent(api::display);
                return null;
            }
        });

        implementationManager.addAction(new ActionAdapter("background", config.getBackgroundActionDescription(),
                Collections.singleton(String.class), Void.TYPE) {
            @Override
            public Object invoke(Aclo api, Object obj, Object... args) {
                String[] inputElems = ((String) args[0]).split("-");

                String action = inputElems[0];

                String[] argsToPass = new String[inputElems.length - 1];
                System.arraycopy(inputElems, 1, argsToPass, 0, argsToPass.length);

                executeAction(action, false, true, argsToPass).ifPresent(api::display);
                return null;
            }
        });

        implementationManager.getApi().setCanExitSupplier(() -> {
            for (BackgroundTask bgTask : backgroundTasks) {
                if (!bgTask.getStatus().equals(BackgroundTask.STATUS_DONE)) {
                    return false;
                }
            }

            return true;
        });
    }

    private void executeExitHook() {
        try {
            implementationManager.executeExitHook();
        } catch (InvocationTargetException e) {
            println(config.getInternalErrorMessage());
        }
    }

    private String answerWithTimeout(String taskName, int millis) {
        boolean useInfiniteTimeout = millis <= 0;
        for (BackgroundTask bgTask : backgroundTasks) {
            if (bgTask.getName().equals(taskName)) {
                synchronized (bgTask.getAnswerLock()) {
                    // May be removed while waiting for lock monitor.
                    if (backgroundTasks.contains(bgTask)) {
                        long lastMessage = System.currentTimeMillis();
                        do {
                            if (pollApiEvents(bgTask.getApi())) {
                                lastMessage = System.currentTimeMillis();
                            }

                            try {
                                Thread.sleep(1);
                            } catch (InterruptedException ignored) {
                            }

                            if (bgTask.getFuture().isDone()) {
                                String returnValue;
                                try {
                                    while (true) {
                                        try {
                                            returnValue = bgTask.getFuture().get();
                                            break;
                                        } catch (InterruptedException ignored) {
                                        }
                                    }
                                } catch (ExecutionException e) {
                                    if (e.getCause() instanceof RuntimeException) {
                                        throw (RuntimeException) e.getCause();
                                    } else {
                                        throw new RuntimeException(e.getCause());
                                    }
                                }

                                backgroundTasks.remove(bgTask);
                                return returnValue;
                            }
                        } while (useInfiniteTimeout || System.currentTimeMillis() - millis < lastMessage);

                        return config.getAnswerCompleteMessage();
                    }
                }
            }
        }

        return config.getBackgroundTaskNotFoundMessage();
    }

    public void run() {
        if (started.getAndSet(true)) {
            throw new IllegalStateException("already started");
        }

        println(config.getGreeting());

        try {
            implementationManager.executeStartHook(implementationManager.getApi());
        } catch (InvocationTargetException e) {
            println(config.getInternalErrorMessage());
        }

        pollApiEvents(implementationManager.getApi());
        pollExitRequests();

        while (!stopped.get()) {
            print(config.getPrompt());
            String input = getInput();

            List<String> inputElems = new ArrayList<>(Arrays.asList(input.split("\\s")));
            inputElems.removeIf(String::isEmpty);

            if (!inputElems.isEmpty()) {
                String action = inputElems.get(0);
                List<String> args = inputElems.subList(1, inputElems.size());

                executeAction(action, true, null, args.toArray(new String[0])).ifPresent(this::println);
            }

            pollExitRequests();
        }

        if (forceStop.get()) {
            System.exit(1);
        } else {
            executorService.shutdown();
        }
    }

    private void pollExitRequests() {
        List<AcloImpl> apis = new ArrayList<>();
        apis.add(implementationManager.getApi());
        backgroundTasks.forEach(task -> apis.add(task.getApi()));

        for (AcloImpl api : apis) {
            EVENT_LOOP:
            for (ApiEvent event : api.getEventQueue()) {
                if (event instanceof ApiExitEvent) {
                    for (BackgroundTask bgTask : backgroundTasks) {
                        if (!bgTask.getStatus().equals(BackgroundTask.STATUS_DONE)) {

                            continue EVENT_LOOP;
                        }
                    }

                    executeExitHook();
                    stopped.set(true);
                    println(config.getFarewell());
                }
            }
        }
    }

    private boolean pollApiEvents(AcloImpl api) {
        List<ApiEvent> removals = new ArrayList<>();

        for (ApiEvent event : api.getEventQueue()) {
            if (event instanceof ApiDisplayEvent) {
                println(((ApiDisplayEvent) event).getMessage());

                removals.add(event);
            } else if (event instanceof ApiQueryEvent) {
                ApiQueryEvent queryEvent = (ApiQueryEvent) event;
                print(queryEvent.getMessage());
                String input = getInput();
                queryEvent.getFuture().complete(input);

                removals.add(event);
            }
        }

        api.getEventQueue().removeAll(removals);
        return !removals.isEmpty();
    }

    private Optional<String> executeAction(String action, boolean watchEvents, Boolean asyncOverride, String... args) {
        try {
            boolean isAsync = Objects.requireNonNullElseGet(asyncOverride,
                    () -> implementationManager.getAction(action, args.length)
                            .map(Action::isBackgroundTask).orElse(false));

            AcloImpl api = isAsync ? new AcloImpl() : implementationManager.getApi();

            Future<String> returnValueFuture;
            synchronized (taskCreationLock) {
                if (stopped.get()) {
                    return Optional.empty();
                } else {
                    returnValueFuture = executorService.submit(
                            () -> implementationManager.executeAction(api, action, args));
                }
            }

            if (isAsync) {
                synchronized (backgroundTasksWriteRenameLock) {
                    backgroundTasks.add(new BackgroundTask(api, getBackgroundTaskName(action), returnValueFuture));
                    return Optional.of(config.getBackgroundTaskScheduledMessage());
                }
            } else {
                while (!returnValueFuture.isDone()) {
                    if (watchEvents) {
                        pollApiEvents(implementationManager.getApi());
                    }
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException ignored) { }
                }
                if (watchEvents) {
                    // Make sure nothing is missed.
                    pollApiEvents(implementationManager.getApi());
                }
            }

            String returnValue;
            try {
                returnValue = returnValueFuture.get();
            } catch (ExecutionException e) {
                throw e.getCause();
            }

            if (returnValue.isEmpty()) {
                return Optional.empty();
            } else {
                return Optional.of(returnValue);
            }
        } catch (InvocationTargetException e) {
            return Optional.of(config.getInternalErrorMessage());
        } catch (MalformedArgumentException e) {
            String expectedParams = StringConverter.convertIterable(e.getExpectedParams(), Class::getSimpleName);
            return Optional.of(config.getMalformedArgumentsMessage(expectedParams));
        } catch (MissingActionException e) {
            return Optional.of(config.getActionNotFoundMessage());
        } catch (Throwable e) {
            if (e instanceof RuntimeException) {
                throw (RuntimeException) e;
            } else {
                throw new RuntimeException(e);
            }
        }
    }

    private String getBackgroundTaskName(String base) {
        NAME_ITERATOR:
        for (int i = 1;; i++) {
            String name = base + "_" + i;

            for (BackgroundTask bgTask : backgroundTasks) {
                if (bgTask.getName().equals(name)) {
                    continue NAME_ITERATOR;
                }
            }

            return name;
        }
    }

    private void print(String msg) {
        implementationManager.getOutputStream().print(msg);
    }

    private void println(String msg) {
        implementationManager.getOutputStream().println(msg);
    }

    private String getInput() {
        return new Scanner(implementationManager.getInputStream()).nextLine();
    }
}
