package org.astropeci.aclo.internal;

/**
 * An event that is awaiting user input.
 */
public abstract class ApiAwaitingEvent extends ApiEvent {
}
