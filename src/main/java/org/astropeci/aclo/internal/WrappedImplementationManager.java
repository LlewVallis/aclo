package org.astropeci.aclo.internal;

import org.astropeci.aclo.*;

import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * A utility for managing the implementation wrapped by this framework.
 */
public class WrappedImplementationManager {

    private final Class<?> implementationClass;
    private final Object instance;
    private final List<Action> actions = new ArrayList<>();
    private Method startHook;
    private Method exitHook;
    private Field outStreamField;
    private Field inStreamField;
    private final AcloImpl api;

    public WrappedImplementationManager(Class<?> implementationClass) throws InvalidSetupException {
        if (implementationClass.isAnnotation()) {
            throw new InvalidSetupException("implementation class (" + implementationClass.getName() +
                    ") is an annotation");
        }

        if (implementationClass.isInterface()) {
            throw new InvalidSetupException("implementation class (" + implementationClass.getName() +
                    ") is an interface");
        }

        if (implementationClass.isArray()) {
            throw new InvalidSetupException("implementation class (" + implementationClass.getName() +
                    ") is an array");
        }

        if (implementationClass.isEnum()) {
            throw new InvalidSetupException("implementation class (" + implementationClass.getName() +
                    ") is an enum");
        }

        this.implementationClass = implementationClass;
        this.api = new AcloImpl();

        try {
            Constructor<?> constructor = implementationClass.getDeclaredConstructor();
            constructor.setAccessible(true);
            instance = constructor.newInstance();
        } catch (NoSuchMethodException e) {
            throw new InvalidSetupException("no no-arg constructor present in implementation class (" +
                    implementationClass.getName() + ")", e);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            throw new InvalidSetupException("could not initialize implementation class (" +
                    implementationClass.getName() + ")", e);
        }

        for (Method method : implementationClass.getDeclaredMethods()) {
            if (method.isAnnotationPresent(AcloAction.class)) {
                Action newAction = new MethodAction(method);

                for (Action action : actions) {
                    AtomicInteger newActionAmountParams = new AtomicInteger();
                    newAction.getRequiredParams().forEach(x -> newActionAmountParams.getAndIncrement());

                    AtomicInteger actionAmountParams = new AtomicInteger();
                    action.getRequiredParams().forEach(x -> actionAmountParams.getAndIncrement());

                    if (newAction.getName().equals(action.getName()) &&
                            newActionAmountParams.equals(actionAmountParams)) {
                        throw new InvalidSetupException("duplicate action: " + action.getName());
                    }
                }

                actions.add(newAction);
            }

            if (method.isAnnotationPresent(AcloStartHook.class)) {
                if (method.getParameterTypes().length > 1) {
                    throw new MalformedMethodException("the start hook takes multiple parameters");
                }

                if (method.getParameterTypes().length == 1) {
                    if (!method.getParameterTypes()[0].equals(Aclo.class)) {
                        throw new MalformedMethodException("the start hook takes one parameter, but it is not an Aclo" +
                                "instance");
                    }
                }

                if (!method.getReturnType().equals(Void.TYPE)) {
                    throw new MalformedMethodException("the start hook does not return void");
                }

                if (startHook == null) {
                    startHook = method;
                } else {
                    throw new InvalidSetupException("duplicate start hooks");
                }
            }

            if (method.isAnnotationPresent(AcloExitHook.class)) {
                if (method.getParameterTypes().length > 0) {
                    throw new MalformedMethodException("the exit hook takes parameters");
                }

                if (!method.getReturnType().equals(Void.TYPE)) {
                    throw new MalformedMethodException("the exit hook does not return void");
                }

                if (exitHook == null) {
                    exitHook = method;
                } else {
                    throw new InvalidSetupException("duplicate exit hooks");
                }
            }
        }

        for (Field field : implementationClass.getDeclaredFields()) {
            if (field.isAnnotationPresent(AcloOutputStreamSource.class)) {
                if (outStreamField == null) {
                    if (field.getType().equals(PrintStream.class)) {
                        outStreamField = field;
                    } else {
                        throw new InvalidSetupException("output stream field is not of appropriate type");
                    }
                } else {
                    throw new InvalidSetupException("duplicate output stream fields");
                }
            }

            if (field.isAnnotationPresent(AcloInputStreamSource.class)) {
                if (inStreamField == null) {
                    if (field.getType().equals(InputStream.class)) {
                        inStreamField = field;
                    } else {
                        throw new MalformedFieldException("input stream field is not of appropriate type");
                    }
                } else {
                    throw new MalformedFieldException("duplicate input stream fields");
                }
            }
        }
    }

    public boolean hasAction(String action, int argAmount) {
        return getAction(action, argAmount).isPresent();
    }

    public Optional<Action> getAction(String action, int argAmount) {
        for (Action candidate : getActions()) {
            List<Class<?>> requiredParams = new ArrayList<>();
            candidate.getRequiredParams().forEach(requiredParams::add);

            if (candidate.getName().equals(action) && requiredParams.size() == argAmount) {
                return Optional.of(candidate);
            }
        }

        return Optional.empty();
    }

    public String executeAction(Aclo api, String action, String... args) throws InvocationTargetException,
            MalformedArgumentException, MissingActionException {
        for (Action candidate : getActions()) {
            List<Class<?>> requiredParams = new ArrayList<>();
            candidate.getRequiredParams().forEach(requiredParams::add);

            if (candidate.getName().equals(action) && requiredParams.size() == args.length) {
                Object[] convertedArgs = convertParams(args, requiredParams);

                try {
                    Object returnValue = candidate.invoke(api, instance, convertedArgs);

                    String returnValueStr = candidate.getReturnType().equals(Void.TYPE)
                            ? ""
                            : Objects.toString(returnValue);

                    return candidate.getResponseMessage(returnValueStr);
                } catch (InvocationTargetException e) {
                    for (Map.Entry<Class<?>, String> entry : candidate.getCaughtExceptions().entrySet()) {
                        if (entry.getKey().isInstance(e.getCause())) {
                            return entry.getValue();
                        }
                    }

                    throw e;
                }
            }
        }

        throw new MissingActionException("the action " + action + " with arguments " + Arrays.toString(args) +
                " could not be found");
    }

    private Object[] convertParams(String[] args, List<Class<?>> requiredParams) throws MalformedArgumentException {
        List<Object> params = new ArrayList<>();

        for (int i = 0; i < args.length; i++) {
            Class<?> type = requiredParams.get(i);
            String arg = args[i];

            if (type.equals(Byte.TYPE)) {
                try {
                    params.add(Byte.parseByte(arg));
                } catch (NumberFormatException e) {
                    throw new MalformedArgumentException("could not convert \"" + arg + "\" to byte", requiredParams);
                }
            } else if (type.equals(Short.TYPE)) {
                try {
                    params.add(Short.parseShort(arg));
                } catch (NumberFormatException e) {
                    throw new MalformedArgumentException("could not convert \"" + arg + "\" to short", requiredParams);
                }
            } else if (type.equals(Integer.TYPE)) {
                try {
                    params.add(Integer.parseInt(arg));
                } catch (NumberFormatException e) {
                    throw new MalformedArgumentException("could not convert \"" + arg + "\" to int", requiredParams);
                }
            } else if (type.equals(Long.TYPE)) {
                try {
                    params.add(Long.parseLong(arg));
                } catch (NumberFormatException e) {
                    throw new MalformedArgumentException("could not convert \"" + arg + "\" to long", requiredParams);
                }
            } else if (type.equals(Float.TYPE)) {
                try {
                    params.add(Float.parseFloat(arg));
                } catch (NumberFormatException e) {
                    throw new MalformedArgumentException("could not convert \"" + arg + "\" to float", requiredParams);
                }
            } else if (type.equals(Double.TYPE)) {
                try {
                    params.add(Double.parseDouble(arg));
                } catch (NumberFormatException e) {
                    throw new MalformedArgumentException("could not convert \"" + arg + "\" to double", requiredParams);
                }
            } else if (type.equals(Boolean.TYPE)) {
                if (arg.equalsIgnoreCase("true") || arg.equalsIgnoreCase("yes")) {
                    params.add(true);
                } else if (arg.equalsIgnoreCase("false") || arg.equalsIgnoreCase("no")) {
                    params.add(false);
                } else {
                    throw new MalformedArgumentException("could not convert \"" + arg + "\" to boolean",
                            requiredParams);
                }
            } else if (type.equals(Character.TYPE)) {
                if (arg.length() == 1) {
                    params.add(arg.charAt(0));
                } else {
                    throw new MalformedArgumentException("could not convert \"" + arg + "\" to char", requiredParams);
                }
            } else {
                params.add(arg);
            }
        }

        return params.toArray();
    }

    public void executeStartHook(Aclo api) throws InvocationTargetException {
        if (startHook != null) {
            try {
                startHook.setAccessible(true);
                if (startHook.getParameterTypes().length == 0) {
                    startHook.invoke(instance);
                } else {
                    startHook.invoke(instance, api);
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void executeExitHook() throws InvocationTargetException {
        if (exitHook != null) {
            try {
                exitHook.invoke(instance);
                exitHook.setAccessible(true);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public Iterable<Action> getActions() {
        return actions;
    }

    public void addAction(Action action) {
        actions.add(action);
    }

    public Class<?> getImplementationClass() {
        return implementationClass;
    }

    public PrintStream getOutputStream() {
        try {
            if (outStreamField == null) return System.out;
            outStreamField.setAccessible(true);
            Object value = outStreamField.get(instance);
            return value == null ? System.out : (PrintStream) value;
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public InputStream getInputStream() {
        try {
            if (inStreamField == null) return System.in;
            inStreamField.setAccessible(true);
            Object value = inStreamField.get(instance);
            return value == null ? System.in : (InputStream) value;
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public AcloImpl getApi() {
        return api;
    }
}
