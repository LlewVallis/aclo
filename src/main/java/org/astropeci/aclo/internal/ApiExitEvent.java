package org.astropeci.aclo.internal;

/**
 * An api event that corresponds to an api exit request.
 */
public class ApiExitEvent extends ApiEvent {
}
