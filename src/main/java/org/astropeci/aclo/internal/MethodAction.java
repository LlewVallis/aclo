package org.astropeci.aclo.internal;

import org.apache.commons.lang3.text.StrSubstitutor;
import org.astropeci.aclo.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An action backed by a method.
 */
public class MethodAction implements Action {

    private final Method method;
    private final AcloAction annotation;
    private final Map<String, String> substitutions;
    private final boolean takesApiArgument;

    public MethodAction(Method method) throws MalformedMethodException {
        if (!method.isAnnotationPresent(AcloAction.class)) {
            throw new MalformedMethodException("method " + method.getName() + " is not annotated with AcloAction");
        }

        this.method = method;
        annotation = method.getAnnotation(AcloAction.class);

        takesApiArgument = method.getParameterTypes().length != 0 && method.getParameterTypes()[0].equals(Aclo.class);

        for (Class<?> paramType : getRequiredParams()) {
            if (!paramType.isPrimitive() && !paramType.equals(String.class)) {
                throw new MalformedMethodException("parameter type " + paramType.getSimpleName() +
                        " was not String or primitive in method " + method.getName());
            }
        }

        substitutions = new HashMap<>();
        substitutions.put("method_name", method.getName());

        for (String reserved : CLIClient.reservedActions) {
            if (getName().equals(reserved)) {
                throw new MalformedMethodException("the name " + getName() + " is reserved");
            }
        }
    }

    @Override
    public Object invoke(Aclo api, Object obj, Object... args) throws InvocationTargetException {
        try {
            Object[] fullArgs;
            if (takesApiArgument) {
                fullArgs = new Object[args.length + 1];
                fullArgs[0] = api;
                System.arraycopy(args, 0, fullArgs, 1, args.length);
            } else {
                fullArgs = args;
            }

            method.setAccessible(true);
            return method.invoke(obj, fullArgs);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Iterable<Class<?>> getRequiredParams() {
        if (takesApiArgument) {
            List<Class<?>> list = Arrays.asList(method.getParameterTypes());
            return list.subList(1, list.size());
        } else {
            return Arrays.asList(method.getParameterTypes());
        }
    }

    @Override
    public Class<?> getReturnType() {
        return method.getReturnType();
    }

    @Override
    public String getName() {
        return new StrSubstitutor(substitutions, "%(", ")").replace(annotation.actionName());
    }

    @Override
    public String getResponseMessage(String returnValue) {
        Map<String, String> substitutions = new HashMap<>(this.substitutions);
        substitutions.put("return_value", returnValue);
        return new StrSubstitutor(substitutions, "%(", ")").replace(annotation.responseMessage());
    }

    @Override
    public String getDescription() {
        return new StrSubstitutor(substitutions, "%(", ")").replace(annotation.description());
    }

    @Override
    public boolean isBackgroundTask() {
        return annotation.async();
    }

    @Override
    public Map<Class<?>, String> getCaughtExceptions() {
        if (method.isAnnotationPresent(AcloCatches.class)) {
            AcloCatches catchesAnnon = method.getAnnotation(AcloCatches.class);
            Map<Class<?>, String> map = new HashMap<>();
            for (AcloCatch catchAnnon : catchesAnnon.value()) {
                map.put(catchAnnon.caught(),
                        new StrSubstitutor(Map.of(), "%(", ")").replace(catchAnnon.responseMessage()));
            }
            return map;
        } else if (method.isAnnotationPresent(AcloCatch.class)) {
            AcloCatch catchAnnon = method.getAnnotation(AcloCatch.class);
            Map<Class<?>, String> map = new HashMap<>();
            map.put(catchAnnon.caught(),
                    new StrSubstitutor(Map.of(), "%(", ")").replace(catchAnnon.responseMessage()));
            return map;
        } else {
            return Map.of();
        }
    }

    public Method getMethod() {
        return method;
    }
}
