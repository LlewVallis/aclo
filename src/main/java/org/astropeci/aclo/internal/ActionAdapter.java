package org.astropeci.aclo.internal;

import org.astropeci.aclo.Aclo;

import java.util.Map;

/**
 * An action that can be customized easily.
 */
public class ActionAdapter implements Action {

    private final String name;
    private final String description;
    private final Iterable<Class<?>> requiredParams;
    private final Class<?> returnType;

    public ActionAdapter(String name, String description, Iterable<Class<?>> requiredParams, Class<?> returnType) {
        this.name = name;
        this.description = description;
        this.requiredParams = requiredParams;
        this.returnType = returnType;
    }

    @Override
    public Object invoke(Aclo api, Object obj, Object... args) {
        return null;
    }

    @Override
    public Iterable<Class<?>> getRequiredParams() {
        return requiredParams;
    }

    @Override
    public Class<?> getReturnType() {
        return returnType;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getResponseMessage(String returnValue) {
        return returnValue;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean isBackgroundTask() {
        return false;
    }

    @Override
    public Map<Class<?>, String> getCaughtExceptions() {
        return Map.of();
    }
}
