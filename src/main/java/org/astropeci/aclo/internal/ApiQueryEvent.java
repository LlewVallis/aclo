package org.astropeci.aclo.internal;

import java.util.concurrent.CompletableFuture;

/**
 * An event that corresponds to a query api request.
 */
public class ApiQueryEvent extends ApiAwaitingEvent {

    private final String msg;
    private final CompletableFuture<String> future;

    public ApiQueryEvent(String msg, CompletableFuture<String> future) {
        this.msg = msg;
        this.future = future;
    }

    public String getMessage() {
        return msg;
    }

    public CompletableFuture<String> getFuture() {
        return future;
    }
}
