package org.astropeci.aclo.internal;

import org.astropeci.aclo.Aclo;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * An invokable action.
 */
public interface Action {

    Object invoke(Aclo api, Object obj, Object... args) throws InvocationTargetException;

    Iterable<Class<?>> getRequiredParams();

    Class<?> getReturnType();

    String getName();

    String getResponseMessage(String returnValue);

    String getDescription();

    boolean isBackgroundTask();

    Map<Class<?>, String> getCaughtExceptions();
}
