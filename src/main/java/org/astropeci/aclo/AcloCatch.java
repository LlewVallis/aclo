package org.astropeci.aclo;

import java.lang.annotation.*;

/**
 * Results in an action having special output when an uncaught exception is thrown.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Repeatable(AcloCatches.class)
public @interface AcloCatch {

    Class<?> caught();
    String responseMessage();
}
