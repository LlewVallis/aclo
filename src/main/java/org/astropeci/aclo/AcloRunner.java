package org.astropeci.aclo;

import org.astropeci.aclo.internal.CLIClient;
import org.astropeci.aclo.internal.WrappedImplementationManager;

/**
 * An entry point for running the framework.
 */
public class AcloRunner {

    private final Configuration config;
    private final WrappedImplementationManager implementationManager;

    public AcloRunner(Class<?> implementationClass, Configuration config) throws InvalidSetupException {
        this.config = config;
        implementationManager = new WrappedImplementationManager(implementationClass);
    }

    public void run() {
        new CLIClient(config, implementationManager).run();
    }

    public static void init(Class<?> implementationClass) {
        try {
            new AcloRunner(implementationClass, new Configuration()).run();
        } catch (InvalidSetupException e) {
            throw new RuntimeException(e);
        }
    }

    public static void init(Class<?> implementationClass, Configuration config) {
        try {
            new AcloRunner(implementationClass, config).run();
        } catch (InvalidSetupException e) {
            throw new RuntimeException(e);
        }
    }
}
