package org.astropeci.aclo;

/**
 * An exception that signifies the framework user configured a method in a way that was invalid.
 */
public class MalformedMethodException extends InvalidSetupException {

    public MalformedMethodException() {
        super();
    }

    public MalformedMethodException(String msg) {
        super(msg);
    }

    public MalformedMethodException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public MalformedMethodException(Throwable cause) {
        super(cause);
    }

    public MalformedMethodException(String msg, Throwable cause, boolean enableSuppression,
                                    boolean writableStackTrace) {
        super(msg, cause, enableSuppression, writableStackTrace);
    }
}