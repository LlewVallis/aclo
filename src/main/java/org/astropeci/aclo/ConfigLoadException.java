package org.astropeci.aclo;

/**
 * An exception that signifies the framework user configured a the config file in a way that was invalid.
 */
public class ConfigLoadException extends InvalidSetupException {

    public ConfigLoadException() {
        super();
    }

    public ConfigLoadException(String msg) {
        super(msg);
    }

    public ConfigLoadException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public ConfigLoadException(Throwable cause) {
        super(cause);
    }

    public ConfigLoadException(String msg, Throwable cause, boolean enableSuppression,
                               boolean writableStackTrace) {
        super(msg, cause, enableSuppression, writableStackTrace);
    }
}