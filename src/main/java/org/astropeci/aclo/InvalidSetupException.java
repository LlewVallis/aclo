package org.astropeci.aclo;

/**
 * An exception that signifies the framework user configured their application in a way that was invalid.
 */
public class InvalidSetupException extends Exception {

    public InvalidSetupException() {
        super();
    }

    public InvalidSetupException(String msg) {
        super(msg);
    }

    public InvalidSetupException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public InvalidSetupException(Throwable cause) {
        super(cause);
    }

    public InvalidSetupException(String msg, Throwable cause, boolean enableSuppression,
                                 boolean writableStackTrace) {
        super(msg, cause, enableSuppression, writableStackTrace);
    }
}