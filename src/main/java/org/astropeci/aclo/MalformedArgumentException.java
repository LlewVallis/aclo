package org.astropeci.aclo;

import java.util.Collections;
import java.util.List;

/**
 * An exception that signifies the framework could not convert a string argument to the appropriate type.
 */
public class MalformedArgumentException extends InvalidSetupException {

    private final List<Class<?>> expectedParams;

    public MalformedArgumentException(List<Class<?>> expectedParams) {
        super();
        this.expectedParams = expectedParams;
    }

    public MalformedArgumentException(String msg, List<Class<?>> expectedParams) {
        super(msg);
        this.expectedParams = expectedParams;
    }

    public MalformedArgumentException(String msg, Throwable cause, List<Class<?>> expectedParams) {
        super(msg, cause);
        this.expectedParams = expectedParams;
    }

    public MalformedArgumentException(Throwable cause, List<Class<?>> expectedParams) {
        super(cause);
        this.expectedParams = expectedParams;
    }

    public MalformedArgumentException(String msg, Throwable cause, boolean enableSuppression,
                                      boolean writableStackTrace, List<Class<?>> expectedParams) {
        super(msg, cause, enableSuppression, writableStackTrace);
        this.expectedParams = expectedParams;
    }

    public List<Class<?>> getExpectedParams() {
        return Collections.unmodifiableList(expectedParams);
    }
}