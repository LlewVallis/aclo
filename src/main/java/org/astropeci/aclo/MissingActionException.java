package org.astropeci.aclo;

/**
 * An exception that signifies the requested action could not be found.
 */
public class MissingActionException extends Exception {

    public MissingActionException() {
        super();
    }

    public MissingActionException(String msg) {
        super(msg);
    }

    public MissingActionException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public MissingActionException(Throwable cause) {
        super(cause);
    }

    public MissingActionException(String msg, Throwable cause, boolean enableSuppression,
                                  boolean writableStackTrace) {
        super(msg, cause, enableSuppression, writableStackTrace);
    }
}