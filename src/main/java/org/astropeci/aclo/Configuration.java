package org.astropeci.aclo;

import org.apache.commons.lang3.text.StrSubstitutor;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A configuration file.
 */
@SuppressWarnings("FieldCanBeLocal")
public class Configuration {

    private String greeting = "Booting CLI...";
    private String farewell = "Exiting CLI...";
    private String actionNotFoundMessage = "Action not found.";
    private String prompt = "ACLO > ";
    private String internalErrorMessage = "An internal error has occurred.";
    private String malformedArgumentsMessage = "Malformed arguments. Required: %(required_params).";
    private String exitActionDescription = "exits the CLI";
    private String forceExitActionDescription = "exits the CLI";
    private String helpActionDescription = "lists all actions registered with the CLI";
    private String backgroundTaskScheduledMessage = "Background task scheduled.";
    private String statusActionDescription = "lists all background tasks running";
    private String noBackgroundTasksRunningMessage = "No background tasks.";
    private String cannotExitDueToBackgroundTasksMessage =
            "Cannot exit because there are still background tasks running.";
    private String answerActionDescription =
            "displays all messages from a background task and then resumes execution";
    private String finishActionDescription = "brings a background task to the foreground";
    private String backgroundTaskNotFoundMessage = "Background task not found.";
    private int answerTimeout = 250;
    private String answerCompleteMessage = "Answer complete, resuming execution.";
    private String renameActionDescription = "renames a background task";
    private String renameCompleteMessage = "Rename complete.";
    private String backgroundTaskAlreadyExistsMessage = "Background task already exists.";
    private String foregroundActionDescription = "runs an action but forces it to run in the foreground";
    private String backgroundActionDescription = "runs an action but forces it to run in the background";

    public Configuration() {}

    /**
     * Determines whether this class was correctly constructed. This will return false if it was constructed from JSON
     * that did not support all required fields.
     */
    public boolean isWellFormed() {
        // No isEmpty() on Iterable.
        return !getMissingProperties().iterator().hasNext();
    }

    /**
     * Gets all properties that were missing when this was constructed.
     */
    public Iterable<String> getMissingProperties() {
        List<String> fieldNames = new ArrayList<>();

        try {
            for (Field field : Configuration.class.getDeclaredFields()) {
                field.setAccessible(true);
                if (field.get(this) == null) {
                    fieldNames.add(field.getName());
                }
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        return fieldNames;
    }

    public String getGreeting() {
        return greeting;
    }

    public String getActionNotFoundMessage() {
        return actionNotFoundMessage;
    }

    public String getPrompt() {
        return prompt;
    }

    public String getInternalErrorMessage() {
        return internalErrorMessage;
    }

    public String getMalformedArgumentsMessage(String requiredParams) {
        Map<String, String> substitutions = new HashMap<>();
        substitutions.put("required_params", requiredParams);
        return new StrSubstitutor(substitutions, "%(", ")").replace(malformedArgumentsMessage);
    }

    public String getFarewell() {
        return farewell;
    }

    public String getExitActionDescription() {
        return exitActionDescription;
    }

    public String getHelpActionDescription() {
        return helpActionDescription;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public void setFarewell(String farewell) {
        this.farewell = farewell;
    }

    public void setActionNotFoundMessage(String actionNotFoundMessage) {
        this.actionNotFoundMessage = actionNotFoundMessage;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public void setInternalErrorMessage(String internalErrorMessage) {
        this.internalErrorMessage = internalErrorMessage;
    }

    public void setMalformedArgumentsMessage(String malformedArgumentsMessage) {
        this.malformedArgumentsMessage = malformedArgumentsMessage;
    }

    public void setExitActionDescription(String exitActionDescription) {
        this.exitActionDescription = exitActionDescription;
    }

    public void setHelpActionDescription(String helpActionDescription) {
        this.helpActionDescription = helpActionDescription;
    }

    public String getBackgroundTaskScheduledMessage() {
        return backgroundTaskScheduledMessage;
    }

    public void setBackgroundTaskScheduledMessage(String backgroundTaskScheduledMessage) {
        this.backgroundTaskScheduledMessage = backgroundTaskScheduledMessage;
    }


    public String getStatusActionDescription() {
        return statusActionDescription;
    }

    public void setStatusActionDescription(String statusActionDescription) {
        this.statusActionDescription = statusActionDescription;
    }

    public String getNoBackgroundTasksRunningMessage() {
        return noBackgroundTasksRunningMessage;
    }

    public void setNoBackgroundTasksRunningMessage(String noBackgroundTasksRunningMessage) {
        this.noBackgroundTasksRunningMessage = noBackgroundTasksRunningMessage;
    }

    public String getCannotExitDueToBackgroundTasksMessage() {
        return cannotExitDueToBackgroundTasksMessage;
    }

    public void setCannotExitDueToBackgroundTasksMessage(String cannotExitDueToBackgroundTasksMessage) {
        this.cannotExitDueToBackgroundTasksMessage = cannotExitDueToBackgroundTasksMessage;
    }

    public String getForceExitActionDescription() {
        return forceExitActionDescription;
    }

    public void setForceExitActionDescription(String forceExitActionDescription) {
        this.forceExitActionDescription = forceExitActionDescription;
    }

    public String getAnswerActionDescription() {
        return answerActionDescription;
    }

    public void setAnswerActionDescription(String answerActionDescription) {
        this.answerActionDescription = answerActionDescription;
    }

    public String getFinishActionDescription() {
        return finishActionDescription;
    }

    public void setFinishActionDescription(String finishActionDescription) {
        this.finishActionDescription = finishActionDescription;
    }

    public String getBackgroundTaskNotFoundMessage() {
        return backgroundTaskNotFoundMessage;
    }

    public void setBackgroundTaskNotFoundMessage(String backgroundTaskNotFoundMessage) {
        this.backgroundTaskNotFoundMessage = backgroundTaskNotFoundMessage;
    }

    public int getAnswerTimeout() {
        return answerTimeout;
    }

    public void setAnswerTimeout(int answerTimeout) {
        this.answerTimeout = answerTimeout;
    }

    public String getAnswerCompleteMessage() {
        return answerCompleteMessage;
    }

    public void setAnswerCompleteMessage(String answerCompleteMessage) {
        this.answerCompleteMessage = answerCompleteMessage;
    }

    public String getRenameActionDescription() {
        return renameActionDescription;
    }

    public void setRenameActionDescription(String renameActionDescription) {
        this.renameActionDescription = renameActionDescription;
    }

    public String getRenameCompleteMessage() {
        return renameCompleteMessage;
    }

    public void setRenameCompleteMessage(String renameCompleteMessage) {
        this.renameCompleteMessage = renameCompleteMessage;
    }

    public String getBackgroundTaskAlreadyExistsMessage() {
        return backgroundTaskAlreadyExistsMessage;
    }

    public void setBackgroundTaskAlreadyExistsMessage(String backgroundTaskAlreadyExistsMessage) {
        this.backgroundTaskAlreadyExistsMessage = backgroundTaskAlreadyExistsMessage;
    }

    public String getForegroundActionDescription() {
        return foregroundActionDescription;
    }

    public void setForegroundActionDescription(String foregroundActionDescription) {
        this.foregroundActionDescription = foregroundActionDescription;
    }

    public String getBackgroundActionDescription() {
        return backgroundActionDescription;
    }

    public void setBackgroundActionDescription(String backgroundActionDescription) {
        this.backgroundActionDescription = backgroundActionDescription;
    }
}
