package org.astropeci.aclo;

/**
 * An exception that signifies the framework user configured a field in a way that was invalid.
 */
public class MalformedFieldException extends InvalidSetupException {

    public MalformedFieldException() {
        super();
    }

    public MalformedFieldException(String msg) {
        super(msg);
    }

    public MalformedFieldException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public MalformedFieldException(Throwable cause) {
        super(cause);
    }

    public MalformedFieldException(String msg, Throwable cause, boolean enableSuppression,
                                   boolean writableStackTrace) {
        super(msg, cause, enableSuppression, writableStackTrace);
    }
}