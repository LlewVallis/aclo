package org.astropeci.aclo;

import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * An interface for user to interact with the services Aclo provides.
 */
public interface Aclo {

    Aclo FAKE = new Aclo() {
        @Override
        public void display(String msg) { }

        @Override
        public Future<String> query(String msg) {
            return new CompletableFuture<>();
        }

        @Override
        public boolean canExit() {
            return false;
        }

        @Override
        public void requestExit() { }

        @Override
        public Aclo silent() {
            return this;
        }
    };

    void display(String msg);

    default void display() {
        display("");
    }

    default void display(Object obj) {
        display(Objects.toString(obj));
    }
    
    default void display(String format, Object... args) {
        display(String.format(format, args));
    }
    
    default void display(Locale locale, String format, Object... args) {
        display(String.format(locale, format, args));
    }

    Future<String> query(String msg);

    default Future<String> query() {
        return query("");
    }

    default Future<String> query(Object obj) {
        return query(Objects.toString(obj));
    }

    default Future<String> query(String format, Object... args) {
        return query(String.format(format, args));
    }

    default Future<String> query(Locale locale, String format, Object... args) {
        return query(String.format(locale, format, args));
    }

    default String queryAndGet(String msg) {
        try {
            return query(msg).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    default String queryAndGet() {
        return queryAndGet("");
    }

    default String queryAndGet(Object obj) {
        return queryAndGet(Objects.toString(obj));
    }

    default String queryAndGet(String format, Object... args) {
        return queryAndGet(String.format(format, args));
    }

    default String queryAndGet(Locale locale, String format, Object... args) {
        return queryAndGet(String.format(locale, format, args));
    }

    boolean canExit();

    void requestExit();

    Aclo silent();
}
