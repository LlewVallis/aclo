module org.astropeci.aclo {
    requires commons.lang3;
    requires gson;
    requires java.sql;
    requires org.apache.commons.io;

    exports org.astropeci.aclo;
}